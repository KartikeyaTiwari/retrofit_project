package in.raghav.apicalldemo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private TextView textviewResult;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textviewResult = findViewById(R.id.text_view_result);

        //put it in seprate class APICLIENT.JAVA (retrofit , gson and other)
        //jsonPlaceHolderApi = ApiClient.createService(); //before generic

        //after generic
        jsonPlaceHolderApi = ApiClient.createService(JsonPlaceHolderApi.class);

        getPost();
        //getComments();
        //createPost();
        //updatePost();
        //deletePostt();

    }

    private void deletePostt() {
        Call<Void> call = jsonPlaceHolderApi.deletePost(5);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                textviewResult.setText("code " + response.code());

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                textviewResult.setText(t.getMessage());
            }
        });
    }

    private void updatePost() {
        Post post = new Post(12, "new title", "new text1");
        Map<String, String> headvalues = new HashMap<>();
        headvalues.put("Map-Header1","head");

        Call<Post> call = jsonPlaceHolderApi.patchPost(headvalues,7,post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()) {
                    textviewResult.setText("Code :" + response.code());
                    return;
                }
                Post postresponse = response.body();
                String content = "";
                content += "Code: " + response.code() + "\n";
                content += "Id: " + postresponse.getId() + "\n";
                content += "user ID: " + postresponse.getUserid() + "\n";
                content += "title: " + postresponse.getTitle() + "\n";
                content += "text: " + postresponse.getText() + "\n\n";

                textviewResult.setText(content);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textviewResult.setText(t.getMessage());
            }
        });

    }

    private void createPost() {
        //Post post = new Post(23,"New Title","New Text");
        Map<String, String> fields = new HashMap<>();
        fields.put("userId", "25");
        fields.put("title", "title2");
        fields.put("body", "text2");

        Call<Post> call = jsonPlaceHolderApi.createPost(23, "new", "nre");
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()) {
                    textviewResult.setText("Code :" + response.code());
                    return;
                }
                Post postresponse = response.body();
                String content = "";
                content += "Code: " + response.code() + "\n";
                content += "Id: " + postresponse.getId() + "\n";
                content += "user ID: " + postresponse.getUserid() + "\n";
                content += "title: " + postresponse.getTitle() + "\n";
                content += "text: " + postresponse.getText() + "\n\n";

                textviewResult.setText(content);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textviewResult.setText(t.getMessage());

            }
        });
    }

    private void getComments() {
        Call<List<Coment>> call = jsonPlaceHolderApi.getComent(3);
        // Call<List<Coment>> call = jsonPlaceHolderApi.getComent("posts/3/comments");
        call.enqueue(new Callback<List<Coment>>() {
            @Override
            public void onResponse(Call<List<Coment>> call, Response<List<Coment>> response) {
                if (!response.isSuccessful()) {
                    textviewResult.setText("Code :" + response.code());
                    return;
                }
                List<Coment> coments = response.body();

                for (Coment coment : coments) {
                    String content = "";
                    content += "Id: " + coment.getId() + "\n";
                    content += "post ID: " + coment.getPostId() + "\n";
                    content += "Mail: " + coment.getEmail() + "\n";
                    content += "Name: " + coment.getName() + "\n\n";
                    content += "text: " + coment.getText() + "\n\n";

                    textviewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Coment>> call, Throwable t) {
                textviewResult.setText(t.getMessage());
            }
        });

    }

    private void getPost() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("userId", "1");
        parameters.put("_sort", "id");
        parameters.put("_order", "desc");

        Call<List<Post>> call = jsonPlaceHolderApi.getPost(parameters);
        call.enqueue(new Callback<List<Post>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (!response.isSuccessful()) {
                    textviewResult.setText("Code :" + response.code());
                    return;
                }
                List<Post> posts = response.body();
                for (Post post : posts) {
                    String content = "";
                    content += "Id: " + post.getId() + "\n";
                    content += "user ID: " + post.getUserid() + "\n";
                    content += "title: " + post.getTitle() + "\n";
                    content += "text: " + post.getText() + "\n\n";

                    textviewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textviewResult.setText(t.getMessage());

            }
        });
    }
}
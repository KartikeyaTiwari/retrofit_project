package in.raghav.apicalldemo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    //private values
    private static Gson gson = new GsonBuilder().serializeNulls().create();
    private static final String baseURL = "https://jsonplaceholder.typicode.com/";

    //for logging and global headers
    private static Interceptor httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    private static OkHttpClient httpclient1 = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @NotNull
        @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request newRequst = originalRequest.newBuilder()
                    .header("Interceptor-Header", "GLOBAL HEADER")
                    .build();
            return chain.proceed(newRequst);
        }
    }).addInterceptor(httpLoggingInterceptor).build();

    //retrofit builder to consume apis
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpclient1)
            .build();

    // before generic
//   public static JsonPlaceHolderApi  createService(){
//            return retrofit.create(JsonPlaceHolderApi.class);
//   }

    //after generic to make dynamic call
    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}

package in.raghav.apicalldemo;

import java.nio.channels.Pipe;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface JsonPlaceHolderApi {
    //process api with query and getting response
    @GET("posts")
    Call<List<Post>> getPost(@Query("userId") Integer[] userId,
                             @Query("_sort") String sort,
                             @Query("_order") String order);

    //passing any combination of parameters to api and getting response
    @GET("posts")
    Call<List<Post>> getPost(@QueryMap Map<String,String> parameters);

    //pasing value to api and getting response
    @GET("posts/{id}/comments")
    Call<List<Coment>> getComent(@Path("id") int postId);

    //sometimes we have to pass url instead of parameters and getting response
    @GET
    Call<List<Coment>> getComent(@Url String url);

    @POST("posts")
    Call<Post> createPost(@Body Post post);

    //diffrent way for encoding input //also some api might use it (do more research)
    @FormUrlEncoded
    @POST("posts")
    Call<Post> createPost(
            @Field("userId") int userId,
            @Field("title")  String title,
            @Field("body")   String text
    );

    @FormUrlEncoded
    @POST("posts")
    Call<Post> createPost(@FieldMap Map<String,String> fields);

    @Headers({"Static-Header1: 123","Static-Header2: 456"})
    @PUT("posts/{id}")
    Call<Post> putPost(@Header ("Dynamic-Header") String headvalue,
                       @Path("id") int id,
                       @Body Post post);

    @PATCH("posts/{id}")
    Call<Post> patchPost(@HeaderMap Map<String,String> headers, @Path("id") int id, @Body Post post);

    @DELETE("posts/{id}")
    Call<Void> deletePost(@Path("id") int id);

}
